$(document).ready(function () {
    moment.locale('it');
    if ($.fn.edooinit) {
        $.fn.edooinit({
            select2: {
                theme: 'bootstrap4'
            }
        });
    }

    // BOOTSTRAP 4 FORM VALIDATION
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();


    // WORKAROUD FILLED LABEL NEL MATERIAL DESIGN PLUGIN
    $('.bootstrap-datetimepicker').on('dp.hide', function () {
        if ($(this).val()) {
            $(this).parent().addClass("is-filled");
        } else {
            $(this).parent().removeClass("is-filled")
        }
    });



    $('input').focusin(function (element) {
        $(element.currentTarget).next().show();
    });
    $('input').focusout(function (element) {
        $(element.currentTarget).next().hide();
    });

    // $('.loader').hide();
});

// LISTENER DEL CARICAMENTEO DELLA PAGINA, RIMUOVE L'IMMAGINE DI LOADING
$(window).on('load', function () {

    $('#navbar-filter').click(function () {
        if ($('.filtri-div').is(":visible")) {
            $('.filtri-div').animate({
                height: 'toggle'
            }, 300);
        } else {
            $('.filtri-div').animate({
                height: 'toggle'
            }, 300);
        }
    });

    if (isMobile()) {
        $('.filtri-div').hide();
    }

    $('#esci').click(function () {
        window.history.go(-1);
        // window.history.back();
    });

    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#toTop').removeClass("hide").addClass("show");
        } else {
            $('#toTop').removeClass("show").addClass("hide");
        }
    });

    $("#toTop").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    });
    $('.loader').delay(700).hide();
});

function isMobile() {
    try {
        document.createEvent("TouchEvent");
        debugLog('isMobile: true');
        return true;
    } catch (e) {
        debugLog('isMobile: false');
        return false;
    }
}

function setTitle(args = []) {
    title = '';
    args.forEach(function (index) {
        index = index.substr(0, 1).toUpperCase() + index.substr(1);
        if (title == '') {
            title = index;
        } else {
            title = title + ' - ' + index;
        }

    });
    document.title = title;
}

function createAnimation(text, classes) {
    var loading_object = $("<span>", {
        "id": "loading-animation",
        "css": {
            "font-size": "4rem",
            "text-align": "center",
            "padding": "10px",
            "position": "absolute",
            "font-family": "Anton, sans-serif"
        },
        "class": classes,
        "text": text + "..."
    });

    setInterval(function () {
        $('#loading-animation').removeClass(classes).delay(1100).queue(function (next) {
            $('#loading-animation').addClass(classes);
            next();
        });
    }, 5000);

    return loading_object;
}
