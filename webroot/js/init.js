(function ($) {
    $.fn.edooinit = function (opts) {
        var settings = $.extend({}, $.fn.edooinit.defaults, opts);

        if ($.fn.bootstrapMaterialDesign) {
            var material = $('.right_col').bootstrapMaterialDesign({
                autofill: false
            });
            material.init();
        }
        // console.log(settings.select2.theme);
        $('select').select2({
            width: "100%",
            theme: settings.select2.theme
        });

        if ($.fn.togglebutton) {
            $('.select-togglebutton').togglebutton({
                select2: true,
                button: {
                    'active_class': 'active btn-checkbox-checked'
                }
            });
        }
        return true;
    }

    $.fn.edooinit.defaults = {
        select2: {
            theme: 'bootstrap4'
        },
    };
}(jQuery));
