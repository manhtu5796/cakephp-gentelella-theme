<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($title) ? $title : 'Edoo | Print Page'; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->

    <!-- BOOTSTRAP COMPOSER -->
  <?= $this->Html->css('Gentelella./vendor/twbs/bootstrap/dist/css/bootstrap') ?>
  <?= $this->Html->script('Gentelella./vendor/twbs/bootstrap/dist/js/bootstrap') ?>

  <?php echo $this->fetch('css'); ?>
</head>
<body onload="">
<div class="wrapper">
  <?php echo $this->fetch('content'); ?>
</div>
</body>
</html>
