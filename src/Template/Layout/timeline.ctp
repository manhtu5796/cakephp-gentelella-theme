<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= h($this->fetch('title')) ?></title>
  <?php
  echo $this->Html->meta(
      'favicon.ico',
      '/webroot/img/nolo_icon/favicon.png',
      ['type' => 'icon']
  );
  ?>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- FONT AWESOME -->
  <?= $this->Html->css('Gentelella./vendor/components/font-awesome/css/fontawesome'); ?>
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>

  <!-- JQUERY COMPOSER -->
  <?= $this->Html->script('Gentelella./vendor/components/jquery/jquery.min') ?>
  
  <?= $this->Html->script('/webroot/js/popper.min') ?>
  
  <!-- CUSTOM EDOO -->
  <?= $this->Html->css('Gentelella./css/edoo-custom') ?>
  <?= $this->Html->script('Gentelella./js/edoo-custom') ?>

  <!-- MOMENT COMPOSER -->
  <?= $this->Html->script('Gentelella./vendor/moment/moment/min/moment-with-locales.min') ?>


  <!-- BOOTSTRAP COMPOSER -->
  <?= $this->Html->css('Gentelella./vendor/twbs/bootstrap/dist/css/bootstrap') ?>
  <?= $this->Html->script('Gentelella./vendor/twbs/bootstrap/dist/js/bootstrap') ?>

    <!-- BOOTSTRAP 4 SWITCHER -->
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">  
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>


  <!-- NOTY PLUGIN -->
  <?= $this->Html->script('Gentelella./vendor/needim/noty/lib/noty'); ?>
  <?= $this->Html->css('Gentelella./vendor/needim/noty/lib/noty'); ?>


  <!-- Font Awesome
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
  <?php echo $this->Html->css('Gentelella./vendor/bootstrap-glyphicons/bootstrap4-glyphicons/css/bootstrap-glyphicons.min'); ?>
  <!-- <?php echo $this->Html->css('/webroot/css/font-awesome/css/font-awesome.min'); ?> -->
  <?php echo $this->Html->css('Gentelella./vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min'); ?>
  <?php echo $this->Html->css('Gentelella./css/maps/jquery-jvectormap-2.0.3'); ?>
  
  <?php
    echo $this->Html->script('Gentelella./vendor/jquery-confirm/dist/jquery-confirm.min');
    echo $this->Html->css('Gentelella./vendor/jquery-confirm/css/jquery-confirm');

   //Bootstrap Select plugin
   echo $this->Html->script('/webroot/select2/js/select2.full.min');
   echo $this->Html->css('/webroot/select2/css/select2.min');

   echo $this->Html->script('Gentelella./build/js/jquery.sse');

   //Bootstrap checkbox PLUGIN
   //echo $this->Html->css('Gentelella./vendor/bootstrap-icheck/icheck-bootstrap.min');
   echo $this->Html->script('Gentelella./vendor/bootstrap-checkbox/dist/js/bootstrap-checkbox.min');

  //  echo $this->Html->script('/webroot/fullcalendar-3.8.2/lib/moment.min');
   echo $this->Html->script('/webroot/fullcalendar-3.8.2/fullcalendar');
   echo $this->Html->script('/webroot/fullcalendar-3.8.2/locale/it');
   echo $this->Html->css('/webroot/fullcalendar-3.8.2/fullcalendar');

   echo $this->Html->script("/webroot/js/daterangepicker");
   echo $this->Html->css('/webroot/css/daterangepicker');

  //  echo $this->Html->css('/css/datatables.min');
  //  echo $this->Html->script('/js/datatables.min');

   //JS PER GRAFICI
   echo $this->Html->script('echarts.min');
   ?>

  <?= $this->Html->script("/webroot/plugins/jquery-clock-timepicker/jquery-clock-timepicker.min") ?>


  <!--CSS PRIMARY WEBROOT-->
  <?php echo $this->Html->css('/css/default'); ?>
  <?php echo $this->Html->script('default'); ?>
  <?php echo $this->Html->script('modernizr'); ?>

  <?php echo $this->fetch('css'); ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="nav-sm">
  <div class="loader"></div>
  <div>
    <!-- Header Navbar: style can be found in header.less -->
    <?php echo $this->element('nav-top') ?>
  </div>
  <div>

    <div class="container body">
      <div class="main_container">
        <!-- Left side column. contains the sidebar -->
        <?php //echo $this->element('aside-main-sidebar'); ?>
        <?php echo $this->element('aside/Menu/edoo-menu'); ?>
        <!-- Right side column -->
        <?php echo $this->element('NewElement/aside-right-sidebar'); ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $this->Flash->render(); ?>
          <?php echo $this->Flash->render('auth'); ?>
          <?php echo $this->fetch('content'); ?>
        </div>

        <!-- /page content -->

      </div>
    </div>
  </div>

<!-- Time Ago Plugin -->
<?php echo $this->Html->script('Gentelella./vendor/jquery-TimeAgo/jquery.timeago');?>
<?php echo $this->Html->script('Gentelella./vendor/jquery-TimeAgo/jquery.timeago.it');?>

<!-- FILE INPUT PLUGIN -->
<?php echo $this->Html->script('Gentelella./vendor/bootstrap-fileinput/js/fileinput.min');?>
<?php echo $this->Html->css('Gentelella./vendor/bootstrap-fileinput/css/fileinput.min'); ?>

<!-- Bootstrap Anchor plugin -->
<!-- <?php echo $this->Html->script('/webroot/js/bootstrap/bootstrap-anchor');?>
<?php echo $this->Html->css('/webroot/css/bootstrap/bootstrap-anchor'); ?> -->

<!-- SlimScroll -->
<?php echo $this->Html->script('Gentelella./vendor/fastclick/lib/fastclick'); ?>

<!-- jQuery 2.1.4 -->
<!-- <?php echo $this->Html->script('Gentelella./vendor/Chart.js/dist/Chart.min'); ?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('Gentelella./vendor/bernii/gauge.js/dist/gauge.min'); ?>
SlimScroll -->
<?php echo $this->Html->script('Gentelella./vendor/bootstrap-progressbar/bootstrap-progressbar.min'); ?>

<?php echo $this->Html->script('Gentelella./vendor/edsol/jquery.timeline/dist/timeline.min'); ?>
<?php echo $this->Html->css('Gentelella./vendor/edsol/jquery.timeline/dist/timeline.min'); ?>

<?php echo $this->Html->script('Gentelella./vendor/jquery-confirm/dist/jquery-confirm.min'); ?>
<?php echo $this->Html->css('Gentelella./vendor/jquery-confirm/dist/jquery-confirm.min'); ?>

<?php echo $this->Html->script('Gentelella./vendor/edsol/jquery-contextmenu/dist/jquery.contextMenu'); ?>
<?php echo $this->Html->script('Gentelella./vendor/edsol/jquery-contextmenu/dist/jquery.ui.position.min'); ?>
<?php echo $this->Html->css('Gentelella./vendor/jQuery-contextMenu/dist/jquery.contextMenu.min'); ?>

<?php echo $this->Html->script('/webroot/js/jquery-clock-timepicker.min'); ?>


<?php echo $this->Html->script('Gentelella./build/js/custom'); ?>
<?php echo $this->Html->css('Gentelella./build/css/custom'); ?>


<?php echo $this->fetch('script'); ?>
<?php echo $this->fetch('scriptBottom'); ?>

</body>
</html>
