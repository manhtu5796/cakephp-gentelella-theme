<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($title) ? $title : h($this->fetch('title')); ?></title>
  <?php
  echo $this->Html->meta(
    'favicon.ico',
    '/webroot/img/nolo_icon/favicon.png',
    ['type' => 'icon']
  );
  ?>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- FONT AWESOME -->
  <?= $this->Html->css('Gentelella./vendor/components/font-awesome/css/fontawesome'); ?>
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>

  <!-- JQUERY COMPOSER -->
  <?= $this->Html->script('Gentelella./vendor/components/jquery/jquery.min') ?>
  
  <!-- CUSTOM EDOO -->
  <?= $this->Html->css('Gentelella./css/edoo-custom') ?>
  <?= $this->Html->script('Gentelella./js/edoo-custom') ?>

<!-- MOMENT COMPOSER -->
<?= $this->Html->script('Gentelella./vendor/moment/moment/min/moment.min') ?>

<!-- EMAIL TAG PLUGIN -->
<?php echo $this->Html->script('Gentelella./vendor/multiple-emails/multiple-emails'); ?>
<?php echo $this->Html->css('Gentelella./vendor/multiple-emails/multiple-emails'); ?>

  <!-- BOOTSTRAP COMPOSER -->
  <?= $this->Html->css('Gentelella./vendor/twbs/bootstrap/dist/css/bootstrap') ?>
  <?= $this->Html->script('Gentelella./vendor/twbs/bootstrap/dist/js/bootstrap') ?>


  <!-- NOTY PLUGIN -->
  <?= $this->Html->script('Gentelella./vendor/needim/noty/lib/noty'); ?>
  <?= $this->Html->css('Gentelella./vendor/needim/noty/lib/noty'); ?>


  <!-- Font Awesome
  <?php echo $this->Html->css('Gentelella./vendor/bootstrap-glyphicons/bootstrap4-glyphicons/css/bootstrap-glyphicons.min'); ?>
  
  <?php
  echo $this->Html->script('Gentelella./vendor/jquery-confirm/dist/jquery-confirm.min');
  echo $this->Html->css('Gentelella./vendor/jquery-confirm/css/jquery-confirm');


  echo $this->Html->script('Gentelella./build/js/jquery.sse');

  echo $this->Html->script('Gentelella./js/scrollToTop');
  ?>

  <!-- JQUERY REDIRECT -->
  <script type="text/javascript" src="https://cdn.rawgit.com/mgalante/jquery.redirect/master/jquery.redirect.js"></script>

  <!--CSS PRIMARY WEBROOT-->
  <?php echo $this->Html->css('/css/default'); ?>
  <?php echo $this->Html->script('default'); ?>
  <?php echo $this->Html->script('modernizr'); ?>

  <?php echo $this->fetch('css'); ?>
</head>
<body class="nav-sm">
  <div class="loader"></div>
  
  <div>

    <div class="container body">
      <!-- <div class="main_container"> -->
        <?php echo $this->element('nav-top') ?>

        <?php echo $this->element('aside/edoo-sidebar-menu'); ?>
       
        <?php $this->element('NewElement/events_timeline'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $this->Flash->render(); ?>
          <?php echo $this->Flash->render('auth'); ?>
          <?php echo $this->fetch('content'); ?>
        </div>

        <!-- /page content -->

      <!-- </div> -->
    </div>
  </div>
<?php echo $this->Html->script('Gentelella./build/js/custom'); ?>
<?php echo $this->Html->css('Gentelella./build/css/custom'); ?>


<?php echo $this->fetch('script'); ?>
<?php echo $this->fetch('scriptBottom'); ?>
</body>
<a id="toTop"><i class="fa fa-angle-up"></i></a>
</html>