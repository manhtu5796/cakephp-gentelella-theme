<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($title) ? $title : 'Edoo | Login'; ?></title>
  <?php
  echo $this->Html->meta(
      'favicon.ico',
      '/webroot/img/nolo_icon/favicon.png',
      ['type' => 'icon']
  );
  ?>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

   <?= $this->Html->script('/webroot/gentelella_vendor/components/jquery/jquery.min'); ?>
   
  <!-- Bootstrap 3.3.5 -->
  <?= $this->Html->css('/webroot/gentelella_vendor/twbs/bootstrap/dist/css/bootstrap') ?>
  <?= $this->Html->script('/webroot/gentelella_vendor/twbs/bootstrap/dist/js/bootstrap') ?>
    <!-- NOTY PLUGIN -->
    <?= $this->Html->script('/webroot/gentelella_vendor/needim/noty/lib/noty'); ?>
  <?= $this->Html->css('/webroot/gentelella_vendor/needim/noty/lib/noty'); ?>

  <?= $this->Html->css('login'); ?>
  <?= $this->fetch('css'); ?>


</head>
<body>
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Flash->render('auth'); ?>
  <?php echo $this->fetch('content'); ?>
</body>
</html>
