<?php
use Cake\Auth\FormAuthenticate;
use Cake\Auth\BaseAuthenticate;
use Cake\Utility\Inflector;
$file = '';
if($this->plugin){
    $file = ROOT . DS . 'plugins'.DS.$this->plugin.DS.'src' . DS . 'Template' . DS . 'Element' . DS . 'nav-top.ctp';
}
if(!file_exists($file)){
    $file = ROOT . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'nav-top.ctp';
}

if (file_exists($file)):
    ob_start();
    include_once $file;
    echo ob_get_clean();
else:
  $this->Breadcrumbs->templates([
    'wrapper' => '<ol{{attrs}}>{{content}}</ol>',
    'item' => '<li{{attrs}}><a href="{{url}}"{{innerAttrs}}>{{title}}</a></li>{{separator}}',
    'itemWithoutLink' => '<li{{attrs}}><span{{innerAttrs}}>{{title}}</span></li>{{separator}}',
    'separator' => '<li{{attrs}}><span{{innerAttrs}}>{{separator}}</span></li>'
  ]);
?>
<?php echo $this->Html->css('Gentelella./css/edoo-navbar');?>
<!-- top navigation -->
<nav class="navbar d-print-none">
  <?= $this->element('Navtop/breadcrumb') ?>
  
  <div class="navbar-right-element float-right">
    <span id="navbar-filter"><i class="fas fa-sliders-h"></i></span>
    <span id="navbar-notification"><i class="fas fa-bell"></i></span>
    <span id="navbar-event" data-toggle="modal" data-target="#eventModal"><i class="fas fa-newspaper"></i></span>
    <div class="profile-block">
      <?= $this->element('aside/profile') ?>
    </div>
  </div>
</nav>

<?php endif; ?>