<button class="btn dropdown-toggle btn-circle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <div class="profile">
    <?= $this->Html->image("data:image/png;base64,{$user['foto_profilo']}",['class' => 'profile-image']) ?>
  </div>
</button>

<div class="dropdown profile-dropdown">
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <div class="profile-dropdown-info">
      <div class="color"></div>
      <div class="profile-content">
        <div class="profile">
          <?= $this->Html->image("data:image/png;base64,{$user['foto_profilo']}",['class' => 'profile-image']) ?>
        </div>
        <div class="profile-name"><?= ucfirst($user['username']) ?></div>
      </div>
    </div>
    <div class="profile-dropdown-elements">
      <a class="dropdown-item" href="/Users/Profilo"><?= __("Profilo") ?></a>
      <a class="dropdown-item" href="/Users/logout"><?= __("Esci") ?></a>
    </div>
  </div>
</div>

<style>
.profile-dropdown{
  position: absolute;
  top: 50px;
}

.profile-dropdown-info .color{
  background: #3350ce;
  height: 40px;
  margin-bottom: -25px;
}

.profile-dropdown-info .image-name{
  font-size: 1rem;
  position: center;
  color:white;
  left: 20%;
  position: relative;
  font-weight: bold;
}

.profile-content{
  display: flex;
  margin-left: 10px;
}

.profile-name{
  color: white;
  font-weight: bold;
  font-size: 1rem;
  margin-left: 10px;
}

.profile {
  width: 40px;
  height: 40px;
  position: relative;
  overflow: hidden;
  border-radius: 50%;

}
.profile-image {
  display: inline;
margin: 0 auto;
margin-left: -20%;
width: 50px;
height: 70px;
margin-top: -20px;
}
</style>