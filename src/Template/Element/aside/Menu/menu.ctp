<?= $this->Html->css('Gentelella./css/Menu/menu') ?>

<nav id="menu">
  <ul id="panel-menu">
    <?php
    $user_permission = array_keys($user['lista_ruoli']);
    foreach ($menu_list as $key => $menu) :
      $can_access = false;
      $menu_role_permission = $menu['role_permission'];

      foreach ($user_permission as $role) {
        if ($menu_role_permission[$role] == 'true') {
          $can_access = true;
        } else {
          if (!$can_access) {
            $can_access = false;
          }
        }
      } ?>

      <li>
        <?php if ($can_access) : ?>
          <?= $this->element("aside/Menu/menu_item", ['menu' => $menu, 'user_permission' => $user_permission]) ?>
        <?php endif ?>
      </li>
    <?php endforeach ?>
  </ul>
</nav>

<style>
  .mm-wrapper_sidebar-collapsed .mm-slideout,
  .mm-wrapper_sidebar-expanded .mm-slideout {
    z-index: unset;
  }
</style>

<script>
  var pinned_icon = <?= json_encode((!empty($menu_icon_pinned)) ? $menu_icon_pinned : []) ?>;
  var bottom_quick_url = [];

  <?php if (!empty($user['lista_ruoli']['root']) or !empty($user['lista_ruoli']['admin'])) : ?>
    bottom_quick_url.push("<a href='/phpmyadmin' target='_blank' data-toggle='tooltip' title='<?= __("PhpMyAdmin") ?>'><i class='fa fa-table'></i></a>");
  <?php endif; ?>

  bottom_quick_url.push("<a href='/Users/profilo' data-toggle='tooltip' title='<?= __("Profilo") ?>'><i class='fa fa-user'></i></a>");
  bottom_quick_url.push("<a href='/Users/logout' data-toggle='tooltip' title='<?= __("Esci") ?>'><i class='fas fa-door-open'></i></a>");



  $(function() {
    new Mmenu("#menu", {
      "wrapper": ["bootstrap"],
      "iconbar": {
        "use": true,
        "type": "tabs",
        "top": [
          "<a href='/' data-toggle='tooltip' title='Home'><i class='fa fa-home'></i></a>",
        ],
        "bottom": bottom_quick_url
      },
      // sidebar: {
      //   collapsed: "(min-width: 550px)",
      // },
      "extensions": [
        "pagedim-black",
        "theme-<?= $theme ?>"
      ],
      "navbars": [{
        "position": "top",
        "content": [
          "searchfield",
          "close"
        ]
      }],
      "setSelected": {
        "hover": true,
        "parent": true
      },
      "searchfield": {
        "panel": true,
        "placeholder": "<?= __("Cerca") ?>...",
      },
    }, {});
  });

  $('#menu').ready(function() {
    $.each(pinned_icon, function(index, element) {
      $('.mm-iconbar__top').append($(element));
    });

    $('[data-toggle="tooltip"]').tooltip();
  });

  $('.menu-pin').click(function(event) {
    menu_element = $(event.currentTarget);
    var menu_id = $(menu_element).data("menu-id");
    var element = $(menu_element).parent();

    if (!menu_element.hasClass("green")) {
      var element_clone = element.clone(true);

      $(element_clone).find('.menu-pin').remove();
      $(element_clone).find('.menu_text').remove();

      $(element_clone).attr('menu-id', menu_id);

      $('.mm-iconbar__top').append(element_clone);
      $(element).find('.menu-pin').addClass("green");
      $.get("/Users/setShortCutIconMenu/" + menu_id, function(data, status) {
        console.log("Icon Pin id " + menu_id + ":" + status);
      });
    } else {
      $(element).find('.menu-pin').removeClass("green");
      var element = $('.mm-iconbar__top').find(`[data-menu-id='${menu_id}']`);
      $(element).remove();
      $.get("/Users/removeShortCutIconMenu/" + menu_id, function(data, status) {
        console.log("remove icon id " + menu_id + ":" + status);
      });
    }
  });
</script>