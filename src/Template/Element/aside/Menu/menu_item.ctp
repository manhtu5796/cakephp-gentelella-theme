<span>
  <i class='<?= $menu['font_awesome'] ?>'></i>

  <?php if (empty($menu['action'])) : ?>
    <?= ucfirst(__($menu['name'])) ?>
  <?php else : ?>
    <?= $this->Html->link(ucfirst(__($menu['name'])), [
      'controller' => $menu['controller'],
      'action' => $menu['action']
    ]) ?>
  <?php endif ?>
</span>

<?php if (!empty($menu['children'])) : ?>
  <ul>
    <?php foreach ($menu['children'] as $key => $element) : ?>
      <?php
      $can_load_element = false;
      foreach ($user_permission as $role) {
        if ($element['role_permission'][$role]) {
          $can_load_element = true;
          break;
        }
      } ?>
      <li>
        <?php if ($can_load_element) :
          if (!empty($element['children'])) {
            echo $this->element("aside/Menu/menu_item", ['menu' => $element, 'user_permission' => $user_permission]);
          } else {
            echo $this->element("aside/Menu/menu_element", ['element' => $element]);
          }
        endif ?>
      </li>
    <?php endforeach ?>
  </ul>
<?php endif ?>