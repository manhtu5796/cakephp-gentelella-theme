<?php
$file = '';
if($this->plugin){
    $file = ROOT . DS . 'plugins'.DS.$this->plugin.DS.'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'footer-buttons.ctp';
}
if(!file_exists($file)){
    $file = ROOT . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'footer-buttons.ctp';
}

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
$permessi = $this->request->session()->read('Auth')['User']['permessi'];
?>
<span>

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <?= $this->LanguageSwitcher->renderLanguageSwitcher(); ?>
      <a data-toggle="tooltip" title="Apri Menù" class="openclose_menu" id="menu">
        <span class="fa fa-long-arrow-left" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Settings" href="/Settings/index">
        <span class="fa fa-cogs" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Gestione Utenti" href="/gestione-utenti">
        <span class="fa fa-user" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="/Users/logout">
        <span class="fa fa-power-off" aria-hidden="true"></span>
      </a>
    </div>
  </span>
    <!-- /menu footer buttons -->
<?php } ?>
