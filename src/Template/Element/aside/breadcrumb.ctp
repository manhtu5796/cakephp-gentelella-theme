<?= $this->Html->css("Gentelella./css/breadcrumb") ?>

<div class="breadcrumbs">
  <?= $this->Breadcrumbs->render(
    ['class' => 'breadcrumb'],
    ['separator' => '<i class="fa fa-angle-right"></i>']
  )?>
</div>