<?= $this->Html->css("notification") ?>
<?= $this->Html->script("notification") ?>

<div class="notification__container">
  <div class="clear__all"></div>
  <div id="div_notification"></div>
</div>