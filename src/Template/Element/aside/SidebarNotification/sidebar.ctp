<?= $this->Html->css("edoo-sidebar") ?>

<div id="sidebar_notification">
  <div id="navbuttons" class="row">
    <div class="col-10">
      <nav class="">
        <div class="nav nav-pills nav-justified"" id="nav-tab" role="tablist">
          <a class="btn btn-sm bg-lightgray nav-link active" id="notification-tab" data-toggle="tab" href="#notification" role="tab" aria-controls="notification" aria-selected="true">
            <i class="fas fa-envelope-open-text"></i> <?= __("Notifiche") ?>
          </a>
          <a class="btn btn-sm nav-link disabled" id="action-tab" data-toggle="tab" href="#action" role="tab" aria-controls="action" aria-selected="false">
            <i class="fas fa-list-ul"></i> <?= __("Azioni") ?>
          </a>
          <a class="btn btn-sm nav-link disabled" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="setting" aria-selected="false">
            <i class="fas fa-wrench"></i> <?= __("Impostazioni") ?>
          </a>
        </div>
      </nav>
    </div>
    <div class="col-2">
      <i class="far fa-times-circle pointer sidebar_notification_close"></i>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="sidebar_content tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="notification" role="tabpanel" aria-labelledby="notification-tab">
          <?= $this->element("aside/SidebarNotification/notification")?>
        </div>
        <div class="tab-pane fade" id="action" role="tabpanel" aria-labelledby="action-tab">
          <?= $this->element("aside/SidebarNotification/action")?>
        </div>
        <div class="tab-pane fade" id="setting" role="tabpanel" aria-labelledby="setting-tab">
          <?= $this->element("aside/SidebarNotification/setting")?>
        </div>
      </div>
    </div>

  </div>
</div>

<script>
$(document).ready(function() {
  var slider = $('#sidebar_notification').slideReveal({
    trigger: $("#quick_panel_toggle"),
    push: false,
    overlay: true,
    position: "right",
    speed: 200,
    width: "500px",
    shown: function(slider, trigger){},
    hidden: function(slider, trigger){},
    show: function(slider, trigger){},
    hide: function(slider, trigger){}
  });
  
  $('.sidebar_notification_close').click(function(){
    slider.slideReveal("toggle");
  });

  var sse_sidebar_notification = $.SSE('/Utility/sidebarNotification', {
    onOpen: function (event) {},
    onEnd: function (event) {},
    onError: function (event) {
      console.log('SSE Sidebar Error');
    },
    onMessage: function (event) {
      if(event.data.push !== null){
        createSidebarNotification(jQuery.parseJSON(event.data));
      }
    }
  });
  sse_sidebar_notification.start();
});
</script>