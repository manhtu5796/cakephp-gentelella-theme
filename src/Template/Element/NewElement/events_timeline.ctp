<div id="eventModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><?= _('Ultime operazioni') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="py-2">

          <div class="row">
            <div class="col-auto text-center flex-column d-none d-sm-flex">
              <div class="row h-50">
                <div class="col border-right">&nbsp;</div>
                <div class="col">&nbsp;</div>
              </div>
              <h5 class="m-2">
                <span class="badge badge-pill bg-success">&nbsp;</span>
              </h5>
              <div class="row h-50">
                <div class="col border-right">&nbsp;</div>
                <div class="col">&nbsp;</div>
              </div>
            </div>
            <div class="col py-2">
              <div class="card border-success shadow">
                <div class="card-body">
                  <div class="float-right text-success">Tue, Jan 10th 2019 8:30 AM</div>
                  <h4 class="card-title text-success">Day 2 Sessions</h4>
                  <p class="card-text">Sign-up for the lessons and speakers that coincide with your course syllabus. Meet and greet with instructors.</p>
                  <button class="btn btn-sm btn-outline-secondary" type="button" data-target="#t2_details" data-toggle="collapse">Show Details ▼</button>
                  <div class="collapse border" id="t2_details">
                    <div class="p-2 text-monospace">
                      <div>08:30 - 09:00 Breakfast in CR 2A</div>
                      <div>09:00 - 10:30 Live sessions in CR 3</div>
                      <div>10:30 - 10:45 Break</div>
                      <div>10:45 - 12:00 Live sessions in CR 3</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
</div>

<script>
  $('#eventModal').on('show.bs.modal', function(event) {
    activityLoad();
  });

  function activityLoad(){
    $(".timeline").empty();
    $.ajax({
      type: 'POST',
      url: '/operazioni/getOperazioniJson/'+10,
      dataType: "json",
      cache : false,
        success: function(datas){
          console.log(datas);
          $.each(datas,function(key,data){
            createElement(data);
          });
        },
        error: function (data){
          console.log(data);
        }
    })
  }

  function createElement(data){
    console.log(data);
  }
<<<<<<< HEAD
</script> 
=======
</script> 
>>>>>>> 89a6a93f1379399790cb3e9c889b71bb723657a2
