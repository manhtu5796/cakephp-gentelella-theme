<div class="user-info" defer>
  <?php
    $user = $this->request->session()->read('Auth')['User'];
    $image_url = 'Gentelella./images/admin_image.png';
    //debug($user);exit;
    $image_filename = "user_".$user['id'].".jpeg";
    
    if(file_exists(IMAGE_PROFILE_PATH.$image_filename)){
      $image_url = 'profile_image/'.$image_filename;
    }
    echo $this->Html->image($image_url,['class'=>'profile-img img-thumbnail']);
    
    ?>
  
  <span class="infodata">
    <?= ucwords($user['username']);?>
    <div class="dropdown">
      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= __('Operazioni') ?>
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="#"><?= __('Modifica Profilo') ?></a>
        <a class="dropdown-item" href="#"><?= __('Logout') ?></a>
      </div>
    </div>
  </span>
</div>
